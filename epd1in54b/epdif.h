/** @file epdif.h
 *
 * @brief Header file of epdif.c providing EPD interface functions
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/

#ifndef EPDIF_H
#define EPDIF_H

#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_drv_spi.h"

// Pin level definition
#define LOW             0
#define HIGH            1

typedef struct _EPAPER_pins
{
  uint32_t pinBUSY;
  uint32_t pinRST;
  uint32_t pinDC;
  uint32_t pinSpiCS;
  uint32_t pinSpiSCK;
  uint32_t pinSpiMOSI;
} EPAPER_pins;

void epaper_GPIO_Init(void);
void spi_epd_init();
void epaper_uninit();

void EpdDigitalWriteCallback(uint32_t pin, int value);
int EpdDigitalReadCallback(uint32_t pin);
void EpdDelayMsCallback(unsigned int delaytime);
void EpdSpiTransferCallback(unsigned char data);

#endif /* EPDIF_H */
