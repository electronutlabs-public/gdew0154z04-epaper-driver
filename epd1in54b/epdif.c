/** @file epdif.c
 *
 * @brief Implements EPD interface functions
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "epdif.h"

extern EPAPER_pins epaper_pins;

void epaper_GPIO_Init(void)
{
  nrf_gpio_pin_clear(epaper_pins.pinDC);
  nrf_gpio_pin_clear(epaper_pins.pinRST);
  nrf_gpio_pin_clear(epaper_pins.pinSpiCS);

  nrf_gpio_cfg_output(epaper_pins.pinDC);
  nrf_gpio_cfg_output(epaper_pins.pinRST);
  nrf_gpio_cfg_output(epaper_pins.pinSpiCS);
  nrf_gpio_cfg_input(epaper_pins.pinBUSY, NRF_GPIO_PIN_NOPULL);
}

#define SPI_INSTANCE  1 /**< SPI instance index. */
const nrf_drv_spi_t spi_epd = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */

void spi_epd_init()
{
  nrf_drv_spi_config_t spi_config_epd = NRF_DRV_SPI_DEFAULT_CONFIG;
  spi_config_epd.frequency = NRF_DRV_SPI_FREQ_1M;
  spi_config_epd.mode = NRF_DRV_SPI_MODE_0;
  spi_config_epd.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST;
  spi_config_epd.miso_pin = NRF_DRV_SPI_PIN_NOT_USED;
  spi_config_epd.mosi_pin = epaper_pins.pinSpiMOSI;
  spi_config_epd.sck_pin = epaper_pins.pinSpiSCK;
  spi_config_epd.ss_pin = epaper_pins.pinSpiCS;

  APP_ERROR_CHECK(nrf_drv_spi_init(&spi_epd, &spi_config_epd, NULL, NULL));
}

void epaper_uninit()
{
  nrf_drv_spi_uninit(&spi_epd);
}

void EpdDigitalWriteCallback(uint32_t pin, int value) {
  if (value == HIGH) {
    nrf_gpio_pin_set(pin);
  } else {
    nrf_gpio_pin_clear(pin);
  }
}

int EpdDigitalReadCallback(uint32_t pin) {
  if (nrf_gpio_pin_read(pin)) {
    return HIGH;
  } else {
    return LOW;
  }
}

void EpdDelayMsCallback(unsigned int delaytime) {
  nrf_delay_ms(delaytime);  
}

void EpdSpiTransferCallback(unsigned char data)
{
  nrf_gpio_pin_clear(epaper_pins.pinSpiCS);
  nrf_drv_spi_transfer(&spi_epd, &data, 1, NULL, 0);
  nrf_gpio_pin_set(epaper_pins.pinSpiCS);
}