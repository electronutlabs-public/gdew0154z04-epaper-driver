# GDEW0154Z04-epaper-driver

Driver for epaper display GDEW0154Z04. 

Note: Define EPAPER_pins structure in `main.c` and `extern` it in all the source files where you are using this structure.

eg. 
```
// epaper pins
EPAPER_pins epaper_pins = {
    .pinBUSY = 3,
    .pinRST = 2,
    .pinDC = 28,
    .pinSpiCS = 30,
    .pinSpiSCK = 31,
    .pinSpiMOSI = 29,
};
```

Change pins according to your circuit connections.